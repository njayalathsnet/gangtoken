 
# Ethereum Smart Contract Deployment Instructions 

## GangToken.sol deployment

* Go to Remix IDE https://remix.ethereum.org
* In our contract we are using compiler version 0.4.21 so you can directly use this link https://remix.ethereum.org/#optimize=false&version=soljson-v0.4.21+commit.dfe3193c.js
* Copy and paste code in Remix Ide window
* Install metamask chrome extension https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn
* Log in with ropsten test network wallet
* Reload Remix Ide screen (to use metamask’s injected web3js environment)
* In Remix screen go to “Run” tab and select contract name from the drop down list
* Enter “cap” amount in the box right next to “Create” button
* Press “Create” button
* Confirm the transaction with metamask
* Smart contract will be deployed once this transaction will be successfully mined on ethereum blockchain
* Keep Remix ide open for deploying tokensale contract

## GangTokenSale.sol Deployment

* Copy and paste GangTokenSale.sol code into Remix IDE
* We will need three parameters before we deploy our contract
* rate - Number of token units a buyer gets per wei
* wallet - Address where collected funds will be forwarded to
* token - Address of the token being sold
* In Remix screen go to “Run” tab and select contract name from the drop down list
* Enter these parameters in the box right next to “Create” button
* Press “Create” button
* Confirm the transaction with metamask
* Smart contract will be deployed once this transaction will be successfully mined on ethereum blockchain

## GangTokenSale.sol automate Ether -> token conversation

* To automate ether to token conversation we will need some supply on our sale contract
* We will need to execute “transfer” method from token contract and transfer some tokens to this sale contract
* Once we have some initial supply in smart contract it will automatically convert tokens from ether with current rate, transfer token to investor and send forwarded ether to owner’s account

## GangTokenSale.sol rate change
* For rate change call “setRate” method from GangTokenSale smart contract
