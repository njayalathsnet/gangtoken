pragma solidity ^0.4.18;

import './zeppelin/token/ERC20/PausableToken.sol';
import './zeppelin/token/ERC20/BurnableToken.sol';
import './zeppelin/token/ERC20/Cappedtoken.sol';


contract GangToken is CappedToken, PausableToken, BurnableToken {

    string public constant name                 = "Gangs Token";
    string public constant symbol               = "GTKN";
    uint public constant decimals               = 6;

    function GangToken(uint256 _totalSupply) CappedToken(_totalSupply) public {
        totalSupply_ = _totalSupply;
        balances[msg.sender] = _totalSupply;
        paused = true;
    }
}
