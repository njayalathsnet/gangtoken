 
# Ethereum Smart Contract with Multisig Wallet

##GNOSIS MultiSig Wallet

* We will use multisig wallet contract developed by gnosis.pm
* Install metmask chrome extension from https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn
* Logged in to metamask with ropsten test network
* To create multisig wallet go to https://wallet.gnosis.pm
* Click on “I confirm the terms and conditions”
* On the “wallet” tab click on “Add” button to add new wallet
* On “Deploy new wallet” screen enter data in the fields as your requirements, you have to add owner’s address on this screen and you have to make a note of “Required confirmations”
* Once you fill out all the data in this screen just click “Deploy with Factory” button
* Your multisig wallet will be ready to use after successful confirmation of the transection
* Take a note of your wallet address

## Make Multisig transaction to ethereum smart contract

* To access any of the methods of smart contract with multisig wallet we will have to change ownership from “owner” to “multisig wallet address”. You can do that by calling “transferOwnership” method from Deployed smart contract 
* Open your multisig wallet with https://wallet.gnosis.pm
* Click on wallet Name
* On “Multisig transactions” menu click on Add button
* Enter the address of smart contract where we want to make a transection
* Paste “ABI String” you can do that with Remix IDE, ABI will be available in compile section of Remix IDE (note: we will need an ABI for our constructor function)
* Once you paste ABI on the box you will see all the accessible methods from that smart contract
* Select one of the method from that list of methods and enter all those required parameters and then click on “Send Multisig transaction” and confirm it with metamask
* Once you confirm the transaction you will need more confirmations on this if you have settled it for more than one owners
* Forward multisig wallet address to the other owners, for example https://wallet.gnosis.pm/#/wallet/0xfbf385160d421bb67c8b90cc4aea5cbff809ebdf
* To confirmation they will have to click on wallet name and on “Multisig transactions” they will see recent transaction and confirmations along with “Confirm” Button
* Once the contract meet “Required Confirmations” contract will automatically send that transection to ethereum blockchain,
* No one can change any settings in wallet without “Required Confirmations”
